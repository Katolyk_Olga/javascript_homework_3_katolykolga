// Отримати два числа, m і n. 
// Вивести в консоль усі прості числа в діапазоні від m до n (менше із введених чисел буде m, більше буде n). 
// Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище, вивести повідомлення про помилку і запитати обидва числа знову.

let ask_M = +prompt("Enter number for the start of the range", "");
let ask_N = +prompt("Enter number for the end of the range", "");

while (true) {
    if (ask_M >= ask_N) {
        alert("Sorry, the number for the start of the range must be less than the number for the end of the range");
        ask_M = prompt("Enter number for the start of the range again", "");
        ask_N = prompt("Enter number for the start of the range again", "");
    }
    else {
        break;
    }
}

for (; ask_M < ask_N; ask_M++) {
    if (ask_M == 1) {console.log(`${ask_M} is simple`)}; 
    let i = 2;
    while (i <= ask_M) {
        if (ask_M % i !== 0) {
            i++;
        }
        else if (i == ask_M) {
            console.log(`${ask_M} is simple`);
            // console.log(`Great, the number ${ask_M} is SIMPLE.`);
            break;
        }        
        else {
            // console.log(`Sorry, the number ${ask_M} is NOT simple.`);
            break;
        }
    }
};

